package com;

import java.util.Map;

import com.base.utils.RedisTool;
import com.base.utils.resolver.BootstarpConfigureResolver;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class RedisToolApplication {
	
	public static void main(String[] args) {
//		args = testParamters();
		
		log.info("redis-tool starting ......");
		Map<String ,Object> paramters = new BootstarpConfigureResolver().decode(args);
		RedisTool tool = null;
		try {
			tool = new RedisTool((String)paramters.get(BootstarpConfigureResolver.SYMBOL_REDIS_HOST)
				, (Integer)paramters.get(BootstarpConfigureResolver.SYMBOL_REDIS_PORT)
				, (String)paramters.get(BootstarpConfigureResolver.SYMBOL_REDIS_AUTH)
				, (Integer)paramters.get(BootstarpConfigureResolver.SYMBOL_REDIS_DB) );
		} catch (Exception e) {
			log.warn("init failed ,please check your paramters : " + paramters ,e);
			System.exit(-1);
		}
		
		log.info("connection redis server is success !!!");
		
		try {
			String pathname = (String)paramters.get(BootstarpConfigureResolver.SYMBOL_LOCAL_FILE);
			Object db =paramters.get(BootstarpConfigureResolver.SYMBOL_REDIS_DB);
			if(paramters.get(BootstarpConfigureResolver.SYMBOL_OPT) == null) {//备份数据
				log.info("ready to backup data from redis db " + db + " in " + pathname +" file"+ ",Please wait a moment.....");
				tool.backups(pathname);
				log.info("backup data to redis db " + db + " in " + pathname +" file , success !!!");
			}
			else {
				log.info("ready to restore data in redis db " + db + " from " + pathname + "file ,Please wait a moment.....");
				tool.restore(pathname);
				log.info("restore data to redis db " + db + " in " + pathname +" file , success !!!");
			}
		} catch (Exception e) {
			log.warn("unkonw error , opt failed ！！" ,e);
		}
		
		log.info("system is exit !!");
	}
//
//	private static String[] testParamters() {
//		String[] args = new String[4];
//		args[0] = "-h192.168.31.234";
//		
//		args[3] = "-d9";
//		return args;
//	}
//	
	
}
