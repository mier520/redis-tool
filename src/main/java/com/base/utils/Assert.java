package com.base.utils;

import java.io.File;

public class Assert {

	static public void isNull(Object obj) {
		isNull(obj, "obj must be null ,obj : " + obj);
	}
	
	static public void isNull(Object obj ,String msg) {
		if(obj != null) {
			throw new RuntimeException(msg);
		}
	}
	
	static public void isNull(Object ...objs) {
		isNull("objs or elements in array objs must be null ,objs : " + objs ,objs);
	}
	
	static public void isNull(String msg ,Object ...objs) {
		isNull(objs, msg);
		
		for(int i = 0 ,len = objs.length ;i < len ;++i) {
			isNull(objs[i], msg);
		}
	}
	
	static public void nonNull(Object obj) {
		nonNull(obj, "obj must be not null ,obj : " + obj);
	}
	
	static public void nonNull(Object obj ,String msg) {
		if(obj == null) {
			throw new RuntimeException(msg);
		}
	}
	
	static public void nonNull(Object ...objs) {
		nonNull("objs or elements in array objs must be not null ,objs : " + objs ,objs);
	}
	
	static public void nonNull(String msg ,Object ...objs) {
		nonNull(objs, msg);
		
		for(int i = 0 ,len = objs.length ;i < len ;++i) {
			nonNull(objs[i], msg);
		}
	}
	
	static public void isExistsOfFile(File file) {
		isExistsOfFile(file, "file is not found ,file path : " + file.getAbsolutePath());
	}
	
	static public void isExistsOfFile(File file ,String msg) {
		nonNull(file);
		
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				throw new RuntimeException(msg);
			}
		}
	}
	
	static public void isExistsOfFile(String pathName) {
		nonNull(pathName);
		isExistsOfFile(new File(pathName));
	}
	
	static public void isExistsOfFile(String pathName ,String msg) {
		nonNull(pathName);
		isExistsOfFile(new File(pathName), msg);
	}
}
