package com.base.utils.constract;

import java.util.Objects;
import java.util.Properties;

/**
 * redis tool data backup file constract
 * @author huwei
 *
 */
public interface DataBackupFileConstract {

	/**
	 * 备份文件注释描述符号
	 * 系统将不会解析在该符号之后到换行符之间的所有数据；
	 */
	final static String ANNONATION_SYMBOLS = "#";
	/**
	 * 语句开始
	 * 一条正式的数据必须在hmii符号之后
	 */
	final static String START_SYMBOLS = "HMII";
	/**
	 * 语句结束
	 * 一条正式的数据必须在end符号之前；
	 * 结合上述start_symbols ,就意味着一条正式的语句必须在开始和结束符号之间
	 */
	final static String END_SYMBOLS = "END";
	/**
	 * windows 平台系统中的换行符
	 * 用于拆分每一行的语句
	 */
	final static String NEWLINE_SYMBOLS_OF_WINDOW = "\r\n";
	/**
	 * unix 平台系统中的换行符
	 * 用于拆分每一行的语句
	 */
	final static String NEWLINE_SYMBOLS_OF_UNIX = "\n";
	/**
	 * 数据类型描述符号
	 */
	final static String TYPE_SYMBOLS = "TYPE";
	/**
	 * 
	 */
	final static String KEY_SYMBOLS = "KEY";
	/**
	 * 数据值描述符号
	 */
	final static String VALUE_SYMBOLS = "VALUE";
	/**
	 * 赋值描述符号
	 */
	final static String ASSIGNMENT_SYMBOLS = "=";
	/**
	 * 多个数据之间的分割符号
	 */
	final static String DATA_SEPARATOR_SYMBOLS = "&nbsp;";
	/**
	 * redis 类型
	 */
	final static String TYPE_STRING = "STRING";
	final static String TYPE_LIST = "LIST";
	final static String TYPE_SET = "SET";
	final static String TYPE_ZSET = "ZSET";
	final static String TYPE_HASH = "HASH";
	/**
	 * 获取当前系统平台下的换行符
	 */
	default String getNewLineSymbol() {
		Properties sysProperties = System.getProperties();
		String osName = sysProperties.getProperty("os.name");
		if(Objects.nonNull(osName) && osName.toUpperCase().startsWith("WINDOWS")) {
			return NEWLINE_SYMBOLS_OF_WINDOW;
		}
		return NEWLINE_SYMBOLS_OF_UNIX;
	}
	
}
