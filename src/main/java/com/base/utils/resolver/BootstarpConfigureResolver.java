package com.base.utils.resolver;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import lombok.extern.slf4j.Slf4j;

/**
 * bootstarp configure resolver
 * @author huwei
 *
 */
@Slf4j
public class BootstarpConfigureResolver {

	/**
	 * 设置redis的链接地址
	 */
	final static public String SYMBOL_REDIS_HOST = "-h";
	/**
	 * 设置redis的链接端口
	 */
	final static public String SYMBOL_REDIS_PORT = "-p";
	/**
	 * 设置操作的数据库
	 */
	final static public String SYMBOL_REDIS_DB = "-d";
	/**
	 * 设置本地存储或恢复文件
	 */
	final static public String SYMBOL_LOCAL_FILE = "-f";
	/**
	 * 设置redis认证密码
	 */
	final static public String SYMBOL_REDIS_AUTH = "-a";
	/**
	 * 设置操作选择：
	 * null为备份数据，
	 *      非空为恢复数据；
	 */
	final static public String SYMBOL_OPT = "-o";
	
	final static private String DEFALULT_REDIS_HOST = "127.0.0.1";
	final static private Integer DEFAULT_REDIS_DB = 0;
	final static private Integer DEFAULT_REDIS_PORT = 6379;
	final static private String DEFAULT_REDIS_PASSWORD = null;
	final static private String DEFAULT_LOCAL_FILE = "test";
	
	
	public Map<String,Object> decode(String[] args){
		Map<String ,Object> paramters = new HashMap<String, Object>();
		if(Objects.isNull(args) || args.length == 0) {
			return init();
		}
		for(int i= 0 ,len = args.length ;i < len ;++i) {
			String p = args[i];
			
			if(Objects.isNull(p)) {
				continue;
			}
			if(p.length() > 2) {
				String symbol = p.substring(0,2);
				String pv = p.substring(2);
				pushParamter(paramters ,symbol ,pv);
			}
			else if(p.length() == 2) {
				String symbol = p;
				String pv = args[++i];
				pushParamter(paramters ,symbol ,pv);
			}
		}
		
		return supplementDefaultParamters(paramters);
	}


	private Map<String ,Object> supplementDefaultParamters(Map<String, Object> paramters) {
		if(paramters.size() == 0) {
			return init();
		}
		Map<String ,Object> temp = paramters;
		pushParamter(temp,SYMBOL_REDIS_HOST,DEFALULT_REDIS_HOST);
		pushParamter(temp,SYMBOL_REDIS_PORT,DEFAULT_REDIS_PORT + "");
		pushParamter(temp,SYMBOL_REDIS_DB,DEFAULT_REDIS_DB + "");
		pushParamter(temp,SYMBOL_LOCAL_FILE,DEFAULT_LOCAL_FILE);
		
		return temp;
	}
	
	private void pushParamter(Map<String, Object> paramters,String symbol ,String pv) {
		if(paramters.get(symbol) != null) {
			return;
		}
		
		if(SYMBOL_REDIS_PORT.equals(symbol)) {
			try {
				int port = Integer.parseInt(pv);
				paramters.put(SYMBOL_REDIS_PORT, port);
			} catch (Exception e) {
				log.warn("the paramter port can not switch Integer ,please check this value : " + pv);
			}
		}
		else if(SYMBOL_REDIS_HOST.equals(symbol)) {
			paramters.put(SYMBOL_REDIS_HOST, pv);
		}
		else if(SYMBOL_REDIS_DB.equals(symbol)) {
			try {
				int db = Integer.parseInt(pv);
				paramters.put(SYMBOL_REDIS_DB, db);
			} catch (Exception e) {
				log.warn("the paramter redis db can not switch Integer ,please check this value : " + pv);
			}
		}
		else if(SYMBOL_REDIS_AUTH.equals(symbol)) {
			paramters.put(SYMBOL_REDIS_AUTH,pv);
		}
		else if(SYMBOL_LOCAL_FILE.equals(symbol)) {
			paramters.put(SYMBOL_LOCAL_FILE,pv);
		}
		else if(SYMBOL_OPT.equals(symbol)) {
			paramters.put(SYMBOL_OPT,pv);
		}
	}


	private Map<String, Object> init() {
		Map<String ,Object> paramters = new HashMap<String, Object>();
		paramters.put(SYMBOL_REDIS_HOST, DEFALULT_REDIS_HOST);
		paramters.put(SYMBOL_REDIS_PORT, DEFAULT_REDIS_PORT);
		paramters.put(SYMBOL_REDIS_DB, DEFAULT_REDIS_DB);
		paramters.put(SYMBOL_LOCAL_FILE, DEFAULT_LOCAL_FILE);
		paramters.put(SYMBOL_REDIS_AUTH, DEFAULT_REDIS_PASSWORD);
		return paramters;
	}
}
