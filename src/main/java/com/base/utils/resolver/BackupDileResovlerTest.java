package com.base.utils.resolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.base.entity.RedisObject;

public class BackupDileResovlerTest {

//	static public void main(String[] args) {
//		decodeStringTest();
//	}

	static public void decodeStringTest() {
		BackupFileResolver bfr = new BackupFileResolver();
		StringBuffer sb = new StringBuffer();
		sb.append("#this is test file\r\n" + "HMII\r\n" + "TYPE=STRING\r\n" + "KEY=HELLO\r\n" + "VALUE=word\r\n"
				+ "END\r\n" + "HMII\r\n" + "TYPE=SET\r\n" + "KEY=LIST\r\n" + "VALUE=set1&nbsp;set2&nbsp;set3\r\n"
				+ "END\r\n" + "HMII\r\n" + "TYPE=LIST\r\n" + "KEY=LIST\r\n" + "VALUE=list1&nbsp;list2&nbsp;list3\r\n"
				+ "END\r\n"
				+ "HMII\r\n" + "TYPE=HASH\r\n" + "KEY=NIHAO\r\n" + "1=v1\r\n" + "k2=v2\r\n" + "END")
				.append("\r\n");
		System.out.println("___ _____________ separator ___ _____________");
		System.out.println(sb.toString());

		List<RedisObject> redisObjectList = bfr.decode(sb.toString());
		System.out.println(redisObjectList);
	}

	static public void encodeTest() {
		BackupFileResolver bfr = new BackupFileResolver();
		String encodeString = bfr.encodeString("hello", "word");
		System.out.println(encodeString);

		System.out.println("___ _____________ separator ___ _____________");

		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "v1");
		map.put("k2", "v2");
		String encodeHash = bfr.encodeHash("nihao", map);
		System.out.println(encodeHash);

		System.out.println("___ _____________ separator ___ _____________");
		List<String> list = new ArrayList<String>();
		list.add("list1");
		list.add("list2");
		list.add("list3");
		System.out.println(bfr.encodeList("list", list));

		System.out.println("___ _____________ separator ___ _____________");
		Set<String> set = new LinkedHashSet<String>();
		set.add("set1");
		set.add("set2");
		set.add("set3");
		System.out.println(bfr.encodeSet("set", set));
	}

}
