package com.base.entity;

import lombok.Data;

/**
 * redis 对象封装
 * @author huwei
 *
 */
@Data
public class RedisObject {
	Object data;
	String key;
	String redisType;
}